# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy


from .common.utils import register_classes, unregister_classes
from .common.gp_utils import get_active_gp_layer_framelist

from . import core


class AAT_OT_next_frame(bpy.types.Operator) :
    bl_idname = "anim.go_to_next_frame"
    bl_label = "Go to next frame"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        bpy.context.scene.frame_set(bpy.context.scene.frame_current+1)
        return {'FINISHED'}


class AAT_OT_previous_frame(bpy.types.Operator) :
    bl_idname = "anim.go_to_previous_frame"
    bl_label = "Go to previous frame"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        bpy.context.scene.frame_set(bpy.context.scene.frame_current-1)
        return {'FINISHED'}

class AAT_OT_frame_seek_mouse_listener(bpy.types.Operator) :
    bl_idname = "anim.frame_seek_listener"
    bl_label = "Frame seek listener"
    bl_options = {'REGISTER', 'UNDO'}

    def modal(self, context, event) :
        if event.type == 'LEFTMOUSE' and event.value == "RELEASE" :
            core.on_mouse_release()
        
        return {'PASS_THROUGH'}

    def execute(self, context):
        print("Adding listener")
        context.window_manager.modal_handler_add(self)
        return {'RUNNING_MODAL'}
    
class AAT_OT_previous_keyframe(bpy.types.Operator) :
    bl_idname = "anim.go_to_previous_keyframe"
    bl_label = "Go to previous keyframe"
    bl_options = {'REGISTER', 'UNDO'}

    def get_next_keyframe(self) :
        frame_list = get_active_gp_layer_framelist()
        current_frame = bpy.context.scene.frame_current

        for frame_n in frame_list :
            if frame_n > current_frame :
                return frame_n
        

    def get_prev_keyframe(self) :
        frame_list = get_active_gp_layer_framelist()
        current_frame = bpy.context.scene.frame_current

        prev_frames = [frame_n for frame_n in frame_list if frame_n < current_frame]

        return max(prev_frames) if len(prev_frames) > 0 else None
        


    def execute(self, context):
        previous_keyframe = self.get_prev_keyframe()
        if previous_keyframe is not None :
            bpy.context.scene.frame_set(previous_keyframe)

        return {'FINISHED'}

class AAT_OT_next_keyframe(bpy.types.Operator) :
    bl_idname = "anim.go_to_next_keyframe"
    bl_label = "Go to next keyframe"
    bl_options = {'REGISTER', 'UNDO'}
        

    def get_next_keyframe(self) :
        frame_list = get_active_gp_layer_framelist()
        current_frame = bpy.context.scene.frame_current

        next_frames = [frame_n for frame_n in frame_list if frame_n > current_frame]

        return min(next_frames) if len(next_frames) > 0 else None
        


    def execute(self, context):
        next_keyframe = self.get_next_keyframe()
        if next_keyframe is not None :
            bpy.context.scene.frame_set(next_keyframe)

        return {'FINISHED'}

classes = [
    AAT_OT_next_frame,
    AAT_OT_previous_frame,
    AAT_OT_frame_seek_mouse_listener,
    AAT_OT_previous_keyframe,
    AAT_OT_next_keyframe,
]

def register() :
    register_classes(classes)

def unregister() :
    unregister_classes(classes)