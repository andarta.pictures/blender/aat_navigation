# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
from .common.gp_utils import get_active_gp_layer_framelist

frame_seek = 0
keyframe_seek = 0
start_frame = 0
listener_activated = False

def start_listener() :
    global listener_activated, start_frame
    if not listener_activated :
        bpy.ops.anim.frame_seek_listener()
        start_frame = bpy.context.scene.frame_current
        listener_activated = True

def on_mouse_release() : 
    global listener_activated, frame_seek, start_frame, keyframe_seek
    listener_activated = False
    frame_seek = 0
    keyframe_seek = 0
    start_frame = 0

def set_value_frame(self, value) :
    global frame_seek, start_frame
    start_listener()
    adjusted_value = value
    frame_seek = adjusted_value
    bpy.context.scene.frame_set(start_frame+adjusted_value)

def get_offset_keyframe(n) :
    global start_frame

    keyframe_list = get_active_gp_layer_framelist()

    previous_keyframes = [frame_n for frame_n in keyframe_list if frame_n < start_frame]
    previous_keyframes.sort()
    next_keyframes = [frame_n for frame_n in keyframe_list if frame_n > start_frame]
    next_keyframes.sort()

    if n == 0 :
        return start_frame
    
    elif n < 0 :
        if len(previous_keyframes) == 0 :
            return start_frame
        
        n_frames = len(previous_keyframes)
        i = max(n_frames+n, 0)
        return previous_keyframes[i]
    
    elif n > 0 :
        if len(next_keyframes) == 0 :
            return start_frame
        
        n_frames = len(next_keyframes) 
        i = min(n, n_frames-1)
        return next_keyframes[i]

def get_value_frame(self) :
    global frame_seek
    return frame_seek

def set_value_keyframe(self, value) :
    global keyframe_seek, start_frame
    start_listener()
    keyframe_seek = value
    keyframe = get_offset_keyframe(value)
    bpy.context.scene.frame_set(keyframe)

def get_value_keyframe(self) :
    global keyframe_seek
    return keyframe_seek

def register() :
    bpy.types.Scene.frame_seek = bpy.props.IntProperty(
        description="Seek a frame", 
        get=get_value_frame, 
        set=set_value_frame
        )
    bpy.types.Scene.keyframe_seek = bpy.props.IntProperty(
        description="Seek a keyframe", 
        get=get_value_keyframe, 
        set=set_value_keyframe
        )

def unregister() :
    del bpy.types.Scene.frame_seek