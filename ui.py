# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy

class Navigation_Panel(bpy.types.Panel):
    """Creates a Panel in the Object properties window"""
    bl_label = "Navigation"
    bl_idname = "OBJECT_PT_navpanel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Andarta"


    def draw(self, context):
        row = self.layout.row()
        row.label(text="Frames : ")
        row.operator("anim.go_to_previous_frame", icon = "TRIA_LEFT", text="")
        row.prop(context.scene, "frame_seek", text="")
        row.operator("anim.go_to_next_frame", icon = "TRIA_RIGHT", text="")

        row = self.layout.row()
        row.label(text="Keyframes : ")
        row.operator("anim.go_to_previous_keyframe", icon = "TRIA_LEFT", text="")
        row.prop(context.scene, "keyframe_seek", text="")
        row.operator("anim.go_to_next_keyframe", icon = "TRIA_RIGHT", text="")


def register() :
    bpy.utils.register_class(Navigation_Panel)

def unregister() :
    bpy.utils.unregister_class(Navigation_Panel)
